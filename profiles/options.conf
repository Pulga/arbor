# vim: set tw=80 et sw=4 sts=4 ts=4 fdm=marker fdr={{{,}}}

# {{{ General fancy things

*/* SUBOPTIONS: APACHE_MODULES ANT_DATA_TYPES ANT_SCRIPTING ANT_TASKS \
    CONTRIB_MODULES COURIER_MODULES ENCODINGS EPIPHANY_EXTENSIONS EXTENSIONS \
    FPRINT_DRIVERS GCC_VERSION GSTREAMER_PLUGINS GIT_REMOTE_HELPERS \
    IM_PROTOCOLS INPUT_DRIVERS KDE_PARTS KIPI_PLUGINS KOPETE_PLUGINS LINGUAS \
    LUA_ABIS MODULES NGINX_MODULES NUT_DRIVERS PLATFORM PLUGINS \
    AMD64_CPU_FEATURES ARM_CPU_FEATURES VIDEO_DRIVERS X86_CPU_FEATURES \
    P2P_NETWORKS PCSC_DRIVERS PHP_ABIS PHP_EXTENSIONS POSTGRESQL_EXTENSIONS \
    PYTHON_ABIS PROVIDERS RUBY_ABIS VALA_ABIS LIBC COCKPIT_COMPONENTS \
    POSTGRESQL_SERVERS

# Things in HIDDEN_SUBOPTIONS need to go in SUBOPTIONS too
*/* HIDDEN_SUBOPTIONS: PLATFORM AMD64_CPU_FEATURES ARM_CPU_FEATURES \
    X86_CPU_FEATURES LIBC

# suboptions in SUBOPTIONS_NO_DESCRIBE need to be listed in SUBOPTIONS as well.
# These suboptions will not have their descriptions displayed by default.
*/* SUBOPTIONS_NO_DESCRIBE: LINGUAS

# }}}

# {{{ Global defaults

# Ciaran McCreesh <ciaran.mccreesh@googlemail.com>
# PLATFORM and *_CPU_FEATURES values get unmasked in subprofiles
*/* PLATFORM: (-amd64) (-arm) (-x86)
*/* X86_CPU_FEATURES: (-3dnow) (-3dnowext) (-avx) (-avx2) (-fma3) (-fma4) (-mmx) (-mmx2) (-mmxext) (-sse) (-sse2) (-sse3) (-ssse3) (-sse4.1) (-sse4.2) (-sse4a) (-xop)
*/* AMD64_CPU_FEATURES: (-3dnow) (-3dnowext) (-avx) (-avx2) (-fma3) (-fma4) (-mmxext) (-sse3) (-ssse3) (-sse4.1) (-sse4.2) (-sse4a) (-xop)
*/* ARM_CPU_FEATURES: (-iwmmxt) (-neon)
*/* LIBC: (-glibc) (-musl)

# David Leverton <dleverton@exherbo.org>
*/* SUBOPTIONS: MULTIBUILD_C
# Multibuild profiles should unhide this
*/* HIDDEN_SUBOPTIONS: MULTIBUILD_C
# This should list every MULTIBUILD_C value used by any profile.
# Non-multibuild profiles should force on the one they support;
# multibuild profiles should unmask the ones they support and enable
# the appropriate default.
*/* MULTIBUILD_C: (-32) (-64)

# Enable parts by default
*/* PARTS: binaries configuration data development documentation libraries

# Alexander Færøy <ahf@exherbo.org>:
# Sane defaults for X11
*/* INPUT_DRIVERS: keyboard mouse
*/* VIDEO_DRIVERS: vesa

# Wulf C. Krueger <philantrop@exherbo.org>
# Default Apache modules to make it work with its default httpd.conf.
*/* APACHE_MODULES: alias authz_host dir log_config logio mime unixd

# Pierre Lejeune <superheron@gmail.com>
# Drivers for Fprint library
*/* FPRINT_DRIVERS: aes1610 aes2501 aes4000 fdu2000 upeke2 upeksonly upektc upekts uru4000 vcom5s

# Pierre Lejeune <superheron@gmail.com>
# P2P networks supported by MLDonkey
*/* P2P_NETWORKS: bittorrent directconnect donkey donkeysui fasttrack filetp \
    gnutella gnutella2

# Pierre Lejeune <superheron@gmail.com>
# Pcsc drivers for Belgian eID middleware
*/* PCSC_DRIVERS: acr38u ccid

*/* bash-completion crypt jemalloc journald ncurses openssl pam ssl systemd tcpd truetype vim-syntax zlib

# }}}

# {{{ Cross Compilation Options

# Saleem Abdulrasool <compnerd@compnerd.org>
*/* SUBOPTIONS: TARGETS

# Benedikt Morbach <moben@exherbo.org>
# mask deprecated targets here, they can be unmasked in specific profiles for
# compatibility if those still use them. All other targets should be unmasked
# for cross-compilation at all times
*/* TARGETS: (-arm-exherbo-linux-gnueabi)

# }}}

# {{{ Per-package defaults

# Wulf C. Krueger <philantrop@exherbo.org>, May, 3rd 2014
# Enable the pbin option on paludis as per
# http://lists.exherbo.org/pipermail/exherbo-dev/2014-April/001313.html
sys-apps/paludis pbin

# Wulf C. Krueger <philantrop@exherbo.org>, April, 16th 2016
# emacs is in the stages set and, thus, should use a minimal default configuration
app-editors/emacs -* PROVIDERS: -*

dev-scm/git curl

dev-lang/ruby berkdb gdbm

# Bo Ørsted Andresen <zlin@exherbo.org>
# Decent defaults for kde parts
app-office/calligra  KDE_PARTS: sheets words

# Heiko Becker <heirecka@exherbo.org>
# Install wallpapers for Plasma by default
kde/breeze wallpapers

# libcanberra is checked first upstream and lighter for just playing a
# notification sound than phonon.
kde-frameworks/knotifications canberra
# Matches the spell? dep of kdelibs:4
kde-frameworks/sonnet aspell

# Needed for Plasma 5, which is the only actively maintained KDE Desktop
# vlc seems to be slightly preferred over gstreamer upstream.
media-libs/phonon qt5 vlc
media-libs/phonon-gstreamer qt5
media-libs/phonon-vlc qt5
x11-libs/dbusmenu-qt qt5

# wpa_supplicant[systemd] needs dbus enabled
net-wireless/wpa_supplicant dbus

net-fs/nfs-utils nfsv4
www-servers/lighttpd pcre

# Heiko Becker <heirecka@exherbo.org>
# Matches the behavior of the non scm exheres
server-pim/akonadi mysql

sys-fs/cryptsetup gcrypt -openssl

# Ali Polatel <alip@exherbo.org>
# seccomp user filter is available for x86 and amd64 since Linux-3.5
sys-apps/sydbox (-seccomp)

# Wulf C. Krueger <philantrop@exherbo.org>
# Usually, we hard-enable udev. This option is *solely* to break a dep-cycle between
# systemd->util-linux->systemd. Do NOT introduce new udev options.
sys-apps/util-linux udev

# Jakob Nixdorf <flocke@shadowice.org>
# Same as above, only needed to break the cycle systemd->pciutils->systemd.
sys-apps/pciutils udev

# Wulf C. Krueger <philantrop@exherbo.org>
# Provide sane defaults for the virtual providers
virtual/blas                    PROVIDERS: OpenBLAS
virtual/cron                    PROVIDERS: cronie
virtual/dhcp-client             PROVIDERS: dhcpcd
virtual/javadoc                 PROVIDERS: icedtea7
virtual/kerberos                PROVIDERS: heimdal
virtual/libaacs                 PROVIDERS: libaacs
virtual/mta                     PROVIDERS: sendmail
virtual/mysql                   PROVIDERS: mariadb
virtual/notification-daemon     PROVIDERS: notification-daemon
virtual/pkg-config              PROVIDERS: pkg-config
virtual/syslog                  PROVIDERS: rsyslog
virtual/zathura-pdf             PROVIDERS: zathura-pdf-poppler

# Kylie McClain <somasis@exherbo.org>
# Choose most popular providers for common system utilities
virtual/awk                     PROVIDERS: gnu
virtual/coreutils               PROVIDERS: gnu
virtual/cpio                    PROVIDERS: gnu
virtual/grep                    PROVIDERS: gnu
virtual/gzip                    PROVIDERS: gnu
virtual/man                     PROVIDERS: man
virtual/jdk:1.8                 PROVIDERS: icedtea
virtual/jdk:11.0                PROVIDERS: openjdk
virtual/sed                     PROVIDERS: gnu
virtual/tar                     PROVIDERS: gnu
virtual/unzip                   PROVIDERS: unzip
virtual/wget                    PROVIDERS: gnu

# Quentin "Sardem FF7" Glidic <sardemff7@exherbo.org>
# Sane default for text-based web browsers
app-text/sgmltools-lite         PROVIDERS: w3m
app-text/xmlto                  PROVIDERS: lynx

# Kylie McClain <somasis@exherbo.org>
# Use ffmpeg as the default provider for all packages that allow a choice between ffmpeg/libav
*/*                             PROVIDERS: ffmpeg

# Heiko Becker <heirecka@exherbo.org>
# Use openssl as the default for packages providing the choice between openssl/libressl
*/*                             PROVIDERS: openssl
# Use libjpeg-turbo as the default for packages supporting both libjpeg-turbo and ijg-jpeg
*/*                             PROVIDERS: jpeg-turbo

# Marc-Antoine Perennou <keruspe@exherbo.org>
# Use systemd as the default for packages providing the choice between systemd-udevd/eudev
*/*                             PROVIDERS: systemd

# Marc-Antoine Perennou <keruspe@exherbo.org>
# Use gtk3 by default when there's a choice between gtk3 and gtk2
*/*                             PROVIDERS: gtk3

# Johannes Nixdorf <mixi@exherbo.org>
# Use elfutils as the default provider for packages allowing a choice between elfutils/libelf
*/*                             PROVIDERS: elfutils

# Johannes Nixdorf <mixi@exherbo.org>
# Use libxml2 as the default providers for XML handling
*/*                             PROVIDERS: libxml2

# Timo Gurr <tgurr@exherbo.org>
# Use dhcpcd as the default for packages providing the choice between dhcpcd/dhcp(dhclient)
*/*                             PROVIDERS: dhcpcd

# Heiko Becker <heirecka@exherbo.org>
# Use ImageMagick as the default for packages providing a choice between
# ImageMagick/GraphicsMagick
*/*                             PROVIDERS: imagemagick

# Marc-Antoine Perennou <keruspe@exherbo.org>
# Default to the reference implementation for dbus-daemon
*/*                             PROVIDERS: dbus-daemon

# Timo Gurr <tgurr@exherbo.org>
# Use krb5 as the default for packages providing the choice between heimdal/krb5
*/*                             PROVIDERS: krb5

# Quentin "Sardem FF7" Glidic <sardemff7@exherbo.org>
# Presumed options
dev-libs/libepoxy X
gnome-bindings/gtkmm X
kde-frameworks/* X
x11-dri/mesa X
x11-libs/cogl X
x11-libs/gtk+ X
x11-libs/qtbase gui
x11-libs/qttools gui

# Heiko Becker <heirecka@exherbo.org>
# Default lua version
*/* LUA_ABIS: 5.1

# Paul Seidler <pl.seidler@gmail.com>
# Default python version
*/* PYTHON_ABIS: 3.6

# Quentin "Sardem FF7" Glidic <sardemff7@exherbo.org>
# Default ruby version
*/* RUBY_ABIS: 2.5

# Marc-Antoine Perennou <keruspe@exherbo.org>
# Default php version
*/* PHP_ABIS: 7.2

# Marc-Antoine Perennou <keruspe@exherbo.org>
# Default vala version
*/* VALA_ABIS: 0.42

# Marc-Antoine Perennou <keruspe@exherbo.org>
# Default postgresql version
*/* POSTGRESQL_SERVERS: 11

# Bo Ørsted Andresen <zlin@exherbo.org>
# gcc threads should always be enabled unless a new toolchain is being bootstrapped
sys-devel/gcc threads

# Ridai Govinda Pombo <ridai.govinda@gmail.com>
# Default option for libdv
media-libs/libdv sdl

# Kylie McClain <somasis@exherbo.org>
# Enable CACert patch on dev-libs/nss by default, since we put cacert in the ca-certificates
# by default anyway and it leads to less confusion
dev-libs/nss cacert

# Wulf C. Krueger <philantrop@exherbo.org>
# The bindist option is completely broken
mail-client/thunderbird (-bindist)

# Kylie McClain <somasis@exherbo.org>
# glib-networking wants providers:gnutls for [ssl], and since we
# don't enable providers:gnutls system-wide, we need to be explicit
dev-libs/glib-networking providers: gnutls

# Kylie McClain <somasis@exherbo.org>
# sys-apps/busybox is often used for fixing broken systems, which [static] can be useful for
sys-apps/busybox static

# Julian Ospald <hasufell@posteo.de>
# Set sensible xml parser default, so gerrit doesn't break
dev-games/cegui tinyxml

# Marc-Antoine Perennou <keruspe@exherbo.org>
# Enable gnome-keyring's ssh-agent by default to be backward compatible
gnome-desktop/gnome-keyring ssh-agent

# Ali Polatel <alip@exherbo.org>
# Prefer fast process_vm_{read,write}v system calls rather than ptrace()
# You never want to disable this unless you have no other choice.
# E.g: You may lack the system calls before Linux-3.2 or
# after if CONFIG_CROSS_MEMORY_ATTACH=n
dev-libs/pinktrace pvm

# Rasmus Thomsen <Rasmus.thomsen@protonmail.com>
# Set sane default for which h264 encoder to choose by default
# X264 is faster at encoding and produces better files at lower bitrates
media/ffmpeg PROVIDERS: x264

# Heiko Becker <heirecka@exherbo.org>
# Building against qt5 is the upstream default, qt4 support is deprecated
media-libs/libmygpo-qt qt5
# Simply prefer gdbm because it's in ::arbor and probably more common than
# tokyocabinet
mail-client/mutt gdbm

# Quentin "Sardem FF7" Glidic <sardemff7@exherbo.org>
# Some default to get to work, it is in stages already
net-apps/NetworkManager PROVIDERS: gnutls

# Rasmus Thomsen <cogitri@exherbo.org>
# Prefer cairo to qt5 since it's almost always pulled in for GTK+ etc. anyway
app-text/poppler cairo

# Arthur Nascimento <tureba@gmail.com>
# Berkeley DB has some licensing issues, but has been the default for a while
mail-mta/postfix berkdb

# Kim Højgaard-Hansen <kimrhh@exherbo.org>
# Need a default when building in CI. Iproute2 should be preferred over deprecated tools (net-tools) anyway.
net-misc/openvpn PROVIDERS: iproute2

# Timo Gurr <tgurr@exherbo.org>
# nghttp2 is mainly used for just the library. Disable the systemd option
# to make the CI happy.
net-libs/nghttp2 -systemd

# Gluzskiy Alexandr <sss@sss.chaoslab.ru>
# turn on zeroconf/avahi in kdnssd,  as it is main purpose of kdnssd, but create posibility to avoid avahi at all if needed
kde-frameworks/kdnssd zeroconf

# Rasmus Thomsen <cogitri@exherbo.org>
# Disable tcpd by default for gdm, it's only required for remote-login, which most
# users don't use. Makes CI happy.
gnome-desktop/gdm -tcpd

# Alexander Kapshuna <kapsh@kap.sh>
# Provide at least one audio output for Audacious player.
media-sound/audacious-plugins alsa

# Heiko Becker <heirecka@exherbo.org>
# Upstream defaults to LuaJit over plain lua, the latter is also masked
mail-filter/rspamd luajit

# Heiko Becker <heirecka@exherbo.org>
# Enable python_abis: 2.7 for packages which blacklist=3 and have a hard dep
# python:2.7
app-admin/gamin python_abis: 2.7
app-misc/griffith python_abis: 2.7
app-paludis/paludis-utils python_abis: 2.7
app-text/calibre python_abis: 2.7
dev-db/ldb python_abis: 2.7
dev-db/tdb python_abis: 2.7
dev-lang/llvm python_abis: 2.7
dev-libs/talloc python_abis: 2.7
dev-libs/tevent python_abis: 2.7
dev-libs/zeitgeist python_abis: 2.7
dev-python/BeautifulSoup python_abis: 2.7
dev-python/backports_shutil_get_terminal_size python_abis: 2.7
dev-python/backports_shutil_which python_abis: 2.7
dev-python/buildutils python_abis: 2.7
dev-python/faulthandler python_abis: 2.7
dev-python/functools32 python_abis: 2.7
dev-python/futures python_abis: 2.7
dev-python/Genshi python_abis: 2.7
dev-python/gnuplot-py python_abis: 2.7
dev-python/M2Crypto python_abis: 2.7
dev-python/mechanize python_abis: 2.7
dev-python/nautilus-python python_abis: 2.7
dev-python/pudge python_abis: 2.7
dev-python/pychm python_abis: 2.7
dev-python/pycryptopp python_abis: 2.7
dev-python/pyPdf python_abis: 2.7
dev-python/python-aosd python_abis: 2.7
dev-python/python-swiftclient python_abis: 2.7
dev-python/python2-pythondialog python_abis: 2.7
dev-python/requests-futures python_abis: 2.7
dev-python/sancho python_abis: 2.7
dev-scm/hgshelve python_abis: 2.7
dev-scm/mercurial python_abis: 2.7
dev-scm/stgit python_abis: 2.7
dev-util/imediff2 python_abis: 2.7
games-rpg/gemrb python_abis: 2.7
gnome-bindings/pygtk:2 python_abis: 2.7
gnome-desktop/gcr python_abis: 2.7
gnome-desktop/gnome-doc-utils python_abis: 2.7
media/kodi python_abis: 2.7
media-plugins/mopidy-iris python_abis: 2.7
media-plugins/mopidy-local-images python_abis: 2.7
media-plugins/mopidy-local-sqlite python_abis: 2.7
media-plugins/mopidy-musicbox-webclient python_abis: 2.7
media-plugins/mopidy-scrobbler python_abis: 2.7
media-plugins/mopidy-spotify python_abis: 2.7
media-sound/mopidy python_abis: 2.7
media-sound/pulseaudio-dlna python_abis: 2.7
media-sound/solfege python_abis: 2.7
net/openvswitch python_abis: 2.7
net-analyzer/greenbone-security-assistant python_abis: 2.7
net-fs/samba python_abis: 2.7
net-im/telepathy-glib python_abis: 2.7
net-im/telepathy-haze python_abis: 2.7
net-im/telepathy-idle python_abis: 2.7
net-im/telepathy-mission-control python_abis: 2.7
net-im/telepathy-salut python_abis: 2.7
net-mail/getmail python_abis: 2.7
net-mail/offlineimap python_abis: 2.7
net-nntp/sabnzbd+ python_abis: 2.7
net-p2p/deluge python_abis: 2.7
sys-libs/libc++ python_abis: 2.7
text/stapler python_abis: 2.7
virtualization-lib/spice python_abis: 2.7
web-apps/mailman python_abis: 2.7
x11-misc/zim python_abis: 2.7
# }}}

