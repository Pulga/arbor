# Copyright 2008 Wulf Krueger <philantrop@exherbo.org>
# Copyright 2008, 2009, 2010 Ingmar Vanhassel <ingmar@exherbo.org>
# Copyright 2014-2017 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'cmake-2.6.0.ebuild' from Gentoo, which is:
#     Copyright 1999-2008 Gentoo Foundation.

require bash-completion elisp [ with_opt=true source_directory=Auxiliary ] flag-o-matic
require freedesktop-desktop freedesktop-mime gtk-icon-cache
require option-renames [ renames=[ 'qt5 gui' ] ]

export_exlib_phases src_prepare src_configure src_compile src_test src_install \
                    pkg_postinst pkg_postrm

SUMMARY="Cross platform Make"
HOMEPAGE="https://www.cmake.org"
DOWNLOADS="${HOMEPAGE}/files/v$(ever range 1-2)/${PNV}.tar.gz"

LICENCES="
    BSD-3
    gui? ( LGPL-3 )
"
SLOT="0"
MYOPTIONS="
    doc       [[ description = [ Install HTML documentation and man pages ] ]]
    bootstrap [[ description = [ Use the internal jsoncpp (it needs cmake to build itself) ] ]]
    gui       [[ description = [ Build a Qt5 based GUI to configure projects ] ]]
    vim-syntax
    ( parts: binaries data development documentation )
"

ever at_least 3.12.0-rc3 && MYOPTIONS+=" ncurses"

DEPENDENCIES="
    build:
        doc? ( dev-python/Sphinx[>=1.6.5] )
    build+run:
        app-arch/bzip2
        app-arch/xz
        app-crypt/rhash
        dev-libs/expat[>=2.0.1]
        dev-libs/libuv[>=1.10.0]
        net-misc/curl[>=7.16.4][ssl(+)]
        sys-libs/ncurses
        !bootstrap? ( dev-libs/jsoncpp:=[>=0.15.5] )
        gui? ( x11-libs/qtbase:5 )
"
# TODO: Versions >= 3.5.0 bundle KWIML (Kitware Information Macro Library,
# https://github.com/Kitware/KWIML ). It's currently not used elsewhere,
# handled a bit differently than other bundled dependencies and, most
# importantly, there's no released tarball. So for now we stick with the
# bundled version.

if ever at_least 3.13.0-rc2 ; then
    DEPENDENCIES+="
        build+run:
            app-arch/libarchive[>=3.1.0]
    "
else
    DEPENDENCIES+="
        build+run:
            app-arch/libarchive[>=3.0.0]
    "
fi

if ever at_least 3.12.0-rc3 ; then
    DEPENDENCIES+="
        build+run:
            ncurses? ( sys-libs/ncurses )
    "
else
    DEPENDENCIES+="
        build+run:
            sys-libs/ncurses
    "
fi

BASH_COMPLETIONS=(
    Auxiliary/bash-completion/cmake
    'Auxiliary/bash-completion/cpack cpack'
    'Auxiliary/bash-completion/ctest ctest'
)

# TODO: Tests are broken because of the multiarch migration. One problem is,
# that we probably need to figure out a way to pass our arch prefixed tools to
# the relevant tests. Additionally cmake isn't really verbose about what's
# wrong. So better disable them for now until someone finds time to fix them
# (heirecka).
RESTRICT="test"

VIMFILE="${PN}.vim"

DEFAULT_SRC_PREPARE_PATCHES+=(
    "${FILES}"/${PN}-remove-test-failure-submits-via-network2.patch
)
DEFAULT_SRC_TEST_PARAMS=( ARGS=--verbose )

cmake-build_src_prepare() {
    default

    # Don't install bash-completions, use bash-completion.exlib
    edo sed -e '/^install(FILES cmake cpack/d' \
            -i Auxiliary/bash-completion/CMakeLists.txt
}

cmake-build_src_configure() {
    local host=$(exhost --target)
    local bootstrap_params=(
        --parallel=${EXJOBS:-1}
        --$(option gui || echo "no-")qt-gui
        --system-bzip2
        --system-curl
        --system-expat
        --system-libarchive
        --system-librhash
        --system-zlib
        --system-libuv
        --prefix=/usr/${host}
        --docdir=/../share/doc/${PNVR}
        --datadir=/../share/${PN}
        --mandir=/../share/man
        --xdgdatadir=/../share
        --verbose
        $(ever at_least 3.12.0-rc3 && option doc && echo "--sphinx-info")
        $(option doc && echo "--sphinx-man --sphinx-html")
        $(option bootstrap --no-system-jsoncpp --system-jsoncpp)
        --
        -DCMAKE_C_COMPILER:PATH="${CC}"
        -DCMAKE_CXX_COMPILER:PATH="${CXX}"
        -DCMAKE_C_FLAGS:STRING="${CFLAGS}"
        -DCMAKE_CXX_FLAGS:STRING="${CXXFLAGS}"
        -DCMAKE_AR:PATH="${AR}"
        -DCMAKE_RANLIB:PATH="${RANLIB}"
        -DCMAKE_LIBRARY_PATH:STRING=/usr/${host}/lib
        -DCMAKE_INCLUDE_PATH:STRING=/usr/${host}/include
        -DCMAKE_C_IMPLICIT_INCLUDE_DIRECTORIES:PATH=/usr/$(exhost --target)/include
        -DCMAKE_CXX_IMPLICIT_INCLUDE_DIRECTORIES:PATH=/usr/$(exhost --target)/include
        -DCMAKE_USE_SYSTEM_LIBUV:BOOL=TRUE
        $(option gui && echo "-DCMake_GUI_DISTRIBUTE_WITH_Qt_LGPL=3")
    )
    if ever at_least 3.12.0-rc3 ; then
        bootstrap_params+=(
            -DBUILD_CursesDialog:BOOL=$(option ncurses && echo "TRUE" || echo "FALSE")
        )
    fi

    # TODO(tridactyla): There are still some issues with cross-compiling to a non-native platform
    # related to generating documentation (it tries to run executables it just built).
    edo env                                                                     \
        CC="$(exhost --build)-cc"                                               \
        CXX="$(exhost --build)-c++"                                             \
        CFLAGS="$(print-build-flags CFLAGS)"                                    \
        CXXFLAGS="$(print-build-flags CXXFLAGS)"                                \
        LDFLAGS="$(print-build-flags LDFLAGS)"                                  \
    ./bootstrap "${bootstrap_params[@]}"
}

cmake-build_src_compile() {
    default
    elisp_src_compile
}

cmake-build_src_test() {
    edo bin/ctest -V -E "CTestTestFailedSubmit|Qt4And5AutomocReverse"
}

cmake-build_src_install() {
    default

    bash-completion_src_install
    elisp_src_install

    if option vim-syntax; then
        insinto /usr/share/vim/vimfiles/syntax
        doins "${WORK}"/Auxiliary/vim/syntax/cmake.vim

        insinto /usr/share/vim/vimfiles/indent
        doins "${WORK}"/Auxiliary/vim/indent/cmake.vim
    fi

    expart binaries /usr/$(exhost --target)/bin
    expart data /usr/share
    expart documentation /usr/share/{doc,info,man}
    expart development /usr/share/cmake/include /usr/share/aclocal
}

cmake-build_pkg_postinst() {
    if option gui ; then
        freedesktop-desktop_pkg_postinst
        freedesktop-mime_pkg_postinst
        gtk-icon-cache_pkg_postinst
    fi
}

cmake-build_pkg_postrm() {
    if option gui ; then
        freedesktop-desktop_pkg_postrm
        freedesktop-mime_pkg_postrm
        gtk-icon-cache_pkg_postrm
    fi
}

