# Copyright 2008, 2009 Ali Polatel <alip@exherbo.org>
# Copyright 2010 Ingmar Vanhassel <ingmar@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'setuptools-0.6_rc8-r1.ebuild' from Gentoo, which is:
#   Copyright 1999-2008 Gentoo Foundation

require pypi [ suffix=zip ] setup-py [ import=distutils has_bin=true ]

SUMMARY="Easily download, build, install, upgrade, and uninstall Python packages"
DESCRIPTION="
Setuptools is a fully-featured, actively-maintained, and stable library designed to facilitate
packaging Python projects, where packaging includes:

* Python package and module definitions
* Distribution package metadata
* Test hooks
* Project installation
* Platform-specific details
* Python 3 support
"

LICENCES="MIT"
SLOT="0"
PLATFORMS="~amd64 ~arm ~armv7 ~armv8 ~x86"
MYOPTIONS=""

PYTEST_VER=3.2.0

# TODO: dev-python/pytest-flake8
DEPENDENCIES="
    post:
        dev-python/certifi[>=2016.9.26][python_abis:*(-)?]
    suggestion:
        (
            dev-python/pip[python_abis:*(-)?]
            dev-python/pytest[>=${PYTEST_VER}][python_abis:*(-)?]
            dev-python/pytest-fixture-config[python_abis:*(-)?]
            dev-python/pytest-virtualenv[python_abis:*(-)?]
            dev-python/virtualenv[python_abis:*(-)?]
            dev-python/wheel[python_abis:*(-)?]
            python_abis:2.7? ( dev-python/mock[python_abis:2.7] )
        ) [[ note = [ required for test suite ] ]]
"

DEFAULT_SRC_INSTALL_EXTRA_DOCS=( EasyInstall.txt api_tests.txt pkg_resources.txt setuptools.txt README.txt )

PYTEST_PARAMS=(
    --verbose

    # disable tests that access the network
    --ignore setuptools/tests/test_easy_install.py
    --ignore setuptools/tests/test_packageindex.py

    # a lot of pkg_resources tests fail with ImportMismatchError
    --ignore pkg_resources

    # TODO, last checked: 40.6.3
    --ignore pavement.py
    --ignore setuptools/tests/test_namespaces.py
)

_test_dependencies_satisfied() {
    has_version dev-python/pip[python_abis:$(python_get_abi)] || return 1
    has_version "dev-python/pytest[>=${PYTEST_VER}][python_abis:$(python_get_abi)]" || return 1
    has_version dev-python/pytest-fixture-config[python_abis:$(python_get_abi)] || return 1
    has_version dev-python/pytest-virtualenv[python_abis:$(python_get_abi)] || return 1
    has_version dev-python/virtualenv[python_abis:$(python_get_abi)] || return 1
    has_version dev-python/wheel[python_abis:$(python_get_abi)] || return 1

    if [[ $(python_get_abi) == 2.7 ]]; then
        has_version dev-python/mock[python_abis:2.7] || return 1
    fi

    return 0
}

test_one_multibuild() {
    if [[ ${MULTIBUILD_TARGET} == 3.* ]]; then
        PYTEST_PARAMS+=(
            # AssertionError for no obvious reason
            --ignore setuptools/tests/test_develop.py
        )
    fi

    # avoid a setuptools <--> pytest dependency loop
    if _test_dependencies_satisfied; then
        PYTEST=py.test-${MULTIBUILD_TARGET}
        # 2.7 requires backports.unittest_mock which depends on setuptools
        [[ ${MULTIBUILD_TARGET} == 3.* ]] && PYTHONPATH="$(ls -d build/lib*)" edo ${PYTEST} "${PYTEST_PARAMS[@]}"
    else
        ewarn "Test dependencies are not yet installed, skipping tests"
    fi
}

