# Copyright 2009-2017 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require udev-rules systemd-service

export_exlib_phases src_prepare pkg_postinst

SUMMARY="Userland tools for the device Mapper multipathing driver"
DESCRIPTION="
If you don't know what this is, you don't want it. Unless you want kpartx which
is part of this package.
"

HOMEPAGE="http://christophe.varoqui.free.fr"
#DOWNLOADS="${HOMEPAGE}/${PN}/${PNV}.tar.bz2"
DOWNLOADS="http://git.opensvc.com/?p=${PN}/.git;a=snapshot;h=${REV};sf=tgz -> ${PNV}.tar.gz"

BUGS_TO="philantrop@exherbo.org"

LICENCES="LGPL-2"
SLOT="0"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        dev-libs/json-c
        dev-libs/libaio[>=0.3.107]
        dev-libs/urcu
        sys-apps/systemd
        sys-fs/lvm2[>=2.02.45]
        sys-fs/sysfsutils[>=2.1.0]
"

DEFAULT_SRC_COMPILE_PARAMS=( CC="${CC}" )

WORK=${WORKBASE}/${PN}-${REV:0:7}

multipath-tools_src_prepare() {
    default

    edo sed -i -e "/libudevdir/s:\$(prefix)/\$(SYSTEMDPATH)/udev:${UDEVRULESDIR/rules.d}:" Makefile.inc
    edo sed -i -e "/unitdir/s:\$(prefix)/\$(SYSTEMDPATH)/systemd/system:${SYSTEMDSYSTEMUNITDIR}:" Makefile.inc
    edo sed -i -e "/bindir/s:/sbin:/usr/$(exhost --target)/bin:" Makefile.inc
    edo sed -i -e "s:\(syslibdir\).*= .*:\1 = /usr/$(exhost --target)/lib:" Makefile.inc
    edo sed -i -e "s:\(libdir\).*= .*:\1 = /usr/$(exhost --target)/lib:" Makefile.inc
    edo sed -i -e "s:\(pkgconfdir\).*= .*:\1 = /usr/$(exhost --target)/lib:" Makefile.inc
    edo sed -i -e "s:\(includedir\).*= .*:\1 = /usr/$(exhost --target)/include:" Makefile.inc
    edo sed -i -e "s:pkg-config:${PKG_CONFIG}:" libdmmp/Makefile
    edo sed -i -e "/^# ENABLE_RADOS/s:#::" Makefile.inc
}

multipath-tools_pkg_postinst() {
    local cruft=( /etc/udev/rules.d/{65-multipath,66-kpartx}.rules )
    for file in ${cruft[@]}; do
        if test -f "${file}" ; then
            nonfatal edo rm "${file}" || ewarn "removing ${file} failed"
        fi
    done
}

