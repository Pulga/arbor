From 66d32b914faab19855c91705a48a54ce418453ab Mon Sep 17 00:00:00 2001
From: Calvin Walton <calvin.walton@kepstin.ca>
Date: Tue, 14 Apr 2015 00:44:45 -0400
Subject: [PATCH] nfs-utils: Run rpcgen using the cpp found by configure.

rpcgen normally runs with a hardcoded cpp path of e.g. /lib/cpp,
but not all Linux distributions install a cpp there.

Grab a trick from glibc, and run rpcgen with a cpp-path pointing at
a script; the script then runs the cpp specified in the CPP
environment variable - which we set to $CC -E with appropriate
options.
---
Upstream: http://thread.gmane.org/gmane.linux.nfs/70511

diff -Naur nfs-utils-2.3.3/configure.ac nfs-utils-2.3.3.new/configure.ac
--- nfs-utils-2.3.3/configure.ac	2018-09-06 20:09:08.000000000 +0200
+++ nfs-utils-2.3.3.new/configure.ac	2018-09-28 14:26:25.862588676 +0200
@@ -157,7 +157,7 @@
 	    RPCGEN_PATH=$rpcgen_path
 	fi
 	AC_SUBST(RPCGEN_PATH)
-	AM_CONDITIONAL(CONFIG_RPCGEN, [test "$RPCGEN_PATH" = "internal"])
+	AM_CONDITIONAL(CONFIG_RPCGEN, [test "$RPCGEN_PATH" = ""])
 AC_ARG_ENABLE(uuid,
 	[AC_HELP_STRING([--disable-uuid], 
 		[Exclude uuid support to avoid buggy libblkid. @<:@default=no@:>@])],
diff -Naur nfs-utils-2.3.3/support/export/Makefile.am nfs-utils-2.3.3.new/support/export/Makefile.am
--- nfs-utils-2.3.3/support/export/Makefile.am	2018-09-06 20:09:08.000000000 +0200
+++ nfs-utils-2.3.3.new/support/export/Makefile.am	2018-09-28 14:18:30.621465026 +0200
@@ -22,22 +22,23 @@
 	done
 
 if CONFIG_RPCGEN
-RPCGEN		= $(top_builddir)/tools/rpcgen/rpcgen
-$(RPCGEN):
+RPCGEN_DEP		= $(top_builddir)/tools/rpcgen/rpcgen
+$(RPCGEN_DEP):
 	make -C $(top_srcdir)/tools/rpcgen all
+RPCGEN = CPP='$(CC) -E -x c-header' $(RPCGEN_DEP) -Y $(top_srcdir)/tools/rpcgen
 else
-RPCGEN = @RPCGEN_PATH@
+RPCGEN = CPP='$(CC) -E -x c-header' @RPCGEN_PATH@ -Y $(top_srcdir)/tools/rpcgen
 endif
 
-$(GENFILES_CLNT): %_clnt.c: %.x $(RPCGEN)
+$(GENFILES_CLNT): %_clnt.c: %.x $(RPCGEN_DEP)
 	test -f $@ && rm -rf $@ || true
 	$(RPCGEN) -l -o $@ $<
 
-$(GENFILES_XDR): %_xdr.c: %.x $(RPCGEN)
+$(GENFILES_XDR): %_xdr.c: %.x $(RPCGEN_DEP)
 	test -f $@ && rm -rf $@ || true
 	$(RPCGEN) -c -i 0 -o $@ $<
 
-$(GENFILES_H): %.h: %.x $(RPCGEN)
+$(GENFILES_H): %.h: %.x $(RPCGEN_DEP)
 	test -f $@ && rm -rf $@ || true
 	$(RPCGEN) -h -o $@ $<
 	rm -f $(top_builddir)/support/include/mount.h
diff -Naur nfs-utils-2.3.3/support/nsm/Makefile.am nfs-utils-2.3.3.new/support/nsm/Makefile.am
--- nfs-utils-2.3.3/support/nsm/Makefile.am	2018-09-06 20:09:08.000000000 +0200
+++ nfs-utils-2.3.3.new/support/nsm/Makefile.am	2018-09-28 14:18:30.621465026 +0200
@@ -15,26 +15,27 @@
 BUILT_SOURCES = $(GENFILES)
 
 if CONFIG_RPCGEN
-RPCGEN	= $(top_builddir)/tools/rpcgen/rpcgen
-$(RPCGEN):
-	make -C ../../tools/rpcgen all
+RPCGEN_DEP	= $(top_builddir)/tools/rpcgen/rpcgen
+$(RPCGEN_DEP):
+	make -C $(top_srcdir)/tools/rpcgen all
+RPCGEN = CPP='$(CC) -E -x c-header' $(RPCGEN_DEP) -Y $(top_srcdir)/tools/rpcgen
 else
-RPCGEN = @RPCGEN_PATH@
+RPCGEN = CPP='$(CC) -E -x c-header' @RPCGEN_PATH@ -Y $(top_srcdir)/tools/rpcgen
 endif
 
-$(GENFILES_CLNT): %_clnt.c: %.x $(RPCGEN)
+$(GENFILES_CLNT): %_clnt.c: %.x $(RPCGEN_DEP)
 	test -f $@ && rm -rf $@ || true
 	$(RPCGEN) -l -o $@ $<
 
-$(GENFILES_SVC): %_svc.c: %.x $(RPCGEN)
+$(GENFILES_SVC): %_svc.c: %.x $(RPCGEN_DEP)
 	test -f $@ && rm -rf $@ || true
 	$(RPCGEN) -m -o $@ $<
 
-$(GENFILES_XDR): %_xdr.c: %.x $(RPCGEN)
+$(GENFILES_XDR): %_xdr.c: %.x $(RPCGEN_DEP)
 	test -f $@ && rm -rf $@ || true
 	$(RPCGEN) -c -i 0 -o $@ $<
 
-$(GENFILES_H): %.h: %.x $(RPCGEN)
+$(GENFILES_H): %.h: %.x $(RPCGEN_DEP)
 	test -f $@ && rm -rf $@ || true
 	$(RPCGEN) -h -o $@ $<
 	echo "void sm_prog_1(struct svc_req *, SVCXPRT *);" >> $@
diff -Naur nfs-utils-2.3.3/tests/nsm_client/Makefile.am nfs-utils-2.3.3.new/tests/nsm_client/Makefile.am
--- nfs-utils-2.3.3/tests/nsm_client/Makefile.am	2018-09-06 20:09:08.000000000 +0200
+++ nfs-utils-2.3.3.new/tests/nsm_client/Makefile.am	2018-09-28 14:18:30.622465027 +0200
@@ -17,26 +17,27 @@
 		   ../../support/nsm/libnsm.a $(LIBCAP) $(LIBTIRPC)
 
 if CONFIG_RPCGEN
-RPCGEN	= $(top_builddir)/tools/rpcgen/rpcgen
-$(RPCGEN):
-	make -C ../../tools/rpcgen all
+RPCGEN_DEP	= $(top_builddir)/tools/rpcgen/rpcgen
+$(RPCGEN_DEP):
+	make -C $(top_srcdir)/tools/rpcgen all
+RPCGEN = CPP='$(CC) -E -x c-header' $(RPCGEN_DEP) -Y $(top_srcdir)/tools/rpcgen
 else
-RPCGEN = @RPCGEN_PATH@
+RPCGEN = CPP='$(CC) -E -x c-header' @RPCGEN_PATH@ -Y $(top_srcdir)/tools/rpcgen
 endif
 
-$(GENFILES_CLNT): %_clnt.c: %.x $(RPCGEN)
+$(GENFILES_CLNT): %_clnt.c: %.x $(RPCGEN_DEP)
 	test -f $@ && rm -rf $@ || true
 	$(RPCGEN) -l -o $@ $<
 
-$(GENFILES_SVC): %_svc.c: %.x $(RPCGEN)
+$(GENFILES_SVC): %_svc.c: %.x $(RPCGEN_DEP)
 	test -f $@ && rm -rf $@ || true
 	$(RPCGEN) -m -o $@ $<
 
-$(GENFILES_XDR): %_xdr.c: %.x $(RPCGEN)
+$(GENFILES_XDR): %_xdr.c: %.x $(RPCGEN_DEP)
 	test -f $@ && rm -rf $@ || true
 	$(RPCGEN) -c -o $@ $<
 
-$(GENFILES_H): %.h: %.x $(RPCGEN)
+$(GENFILES_H): %.h: %.x $(RPCGEN_DEP)
 	test -f $@ && rm -rf $@ || true
 	$(RPCGEN) -h -o $@ $<
 
diff -Naur nfs-utils-2.3.3/tools/rpcgen/cpp nfs-utils-2.3.3.new/tools/rpcgen/cpp
--- nfs-utils-2.3.3/tools/rpcgen/cpp	1970-01-01 01:00:00.000000000 +0100
+++ nfs-utils-2.3.3.new/tools/rpcgen/cpp	2018-09-28 14:18:30.622465027 +0200
@@ -0,0 +1,6 @@
+#!/bin/sh
+
+# This script is use solely by rpcgen when run during the build.
+# It allows using CPP from the environment rather than a hardcoded path.
+
+exec ${CPP} "$@" 
diff -Naur nfs-utils-2.3.3/utils/statd/Makefile.am nfs-utils-2.3.3.new/utils/statd/Makefile.am
--- nfs-utils-2.3.3/utils/statd/Makefile.am	2018-09-06 20:09:08.000000000 +0200
+++ nfs-utils-2.3.3.new/utils/statd/Makefile.am	2018-09-28 14:18:30.622465027 +0200
@@ -24,26 +24,27 @@
 EXTRA_DIST = sim_sm_inter.x $(man8_MANS) simulate.c
 
 if CONFIG_RPCGEN
-RPCGEN	= $(top_builddir)/tools/rpcgen/rpcgen
-$(RPCGEN):
-	make -C ../../tools/rpcgen all
+RPCGEN_DEP	= $(top_builddir)/tools/rpcgen/rpcgen
+$(RPCGEN_DEP):
+	make -C $(top_srcdir)/tools/rpcgen all
+RPCGEN = CPP='$(CC) -E -x c-header' $(RPCGEN_DEP) -Y $(top_srcdir)/tools/rpcgen
 else
-RPCGEN = @RPCGEN_PATH@
+RPCGEN = CPP='$(CC) -E -x c-header' @RPCGEN_PATH@ -Y $(top_srcdir)/tools/rpcgen
 endif
 
-$(GENFILES_CLNT): %_clnt.c: %.x $(RPCGEN)
+$(GENFILES_CLNT): %_clnt.c: %.x $(RPCGEN_DEP)
 	test -f $@ && rm -rf $@ || true
 	$(RPCGEN) -l -o $@ $<
 
-$(GENFILES_SVC): %_svc.c: %.x $(RPCGEN)
+$(GENFILES_SVC): %_svc.c: %.x $(RPCGEN_DEP)
 	test -f $@ && rm -rf $@ || true
 	$(RPCGEN) -m -o $@ $<
 
-$(GENFILES_XDR): %_xdr.c: %.x $(RPCGEN)
+$(GENFILES_XDR): %_xdr.c: %.x $(RPCGEN_DEP)
 	test -f $@ && rm -rf $@ || true
 	$(RPCGEN) -c -o $@ $<
 
-$(GENFILES_H): %.h: %.x $(RPCGEN)
+$(GENFILES_H): %.h: %.x $(RPCGEN_DEP)
 	test -f $@ && rm -rf $@ || true
 	$(RPCGEN) -h -o $@ $<
 
