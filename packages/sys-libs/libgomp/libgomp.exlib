# Copyright 2015 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Public License v2

require gnu [ suffix=tar.xz subdir=gcc-${PV} pn=gcc ]
require toolchain-runtime-libraries

export_exlib_phases src_unpack src_configure src_install

SUMMARY="GNU OpenMP Runtime (libgomp)"
if [[ ${PV} == *_pre* ]] ; then
    DOWNLOADS="ftp://gcc.gnu.org/pub/gcc/snapshots/$(ever major)-${PV##*_pre}/gcc-$(ever major)-${PV//*_pre}.tar.bz2"
elif [[ ${PV} == *_rc* ]] ; then
    DOWNLOADS="ftp://gcc.gnu.org/pub/gcc/snapshots/$(ever major)-RC-${PV##*_rc}/gcc-${PV%_rc*}-RC-${PV##*_rc}.tar.bz2"
fi

LICENCES="GPL-2"
SLOT="$(ever major)"

UPSTREAM_RELEASE_NOTES="${HOMEPAGE}gcc-${SLOT}/"
UPSTREAM_CHANGELOG="${UPSTREAM_RELEASE_NOTES}changes.html"

MYOPTIONS="
    doc
    fortran
    (
        linguas:
            be ca da de el eo es fi fr hr id ja nl ru sr sv tr uk vi zh_CN zh_TW
    )
"

DEPENDENCIES="
    build:
        sys-apps/texinfo[>4.4]
        sys-devel/gcc:${SLOT}
        sys-devel/gettext
        doc? (
            app-doc/doxygen[>=1.5.1]
            dev-libs/libxml2
            dev-libs/libxslt
            media-gfx/graphviz
        )
        fortran? ( sys-libs/libgfortran:${SLOT} )
"

# Those tests previously only ran during gcc's test_expensive phase, however
# they fail completely now:  "... unexpected failures: 436 ..." (5.2.0)
RESTRICT="test"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-clocale=gnu
    --disable-libstdcxx-pch
    --disable-vtable-verify
)

if [[ ${PV} == *_pre* ]] ; then
    ECONF_SOURCE="${WORKBASE}/gcc-$(ever major)-${PV##*_pre}/libgomp"
else
    ECONF_SOURCE="${WORKBASE}/gcc-${PV/_p?(re)/-}/libgomp"
fi
WORK="${WORKBASE}/build/$(exhost --target)/libgomp"

libgomp_src_unpack() {
    default
    edo mkdir -p "${WORK}"
}

libgomp_src_configure() {
    # Similarly to CC/CPP/CXX, we could use GFORTRAN here, but currently this env variable
    # has no effect, so use ac_cv_prog_FC to override.
    option fortran && export ac_cv_prog_FC=$(exhost --tool-prefix)gfortran-${SLOT}
    CC=$(exhost --tool-prefix)gcc-${SLOT}       \
    CPP=$(exhost --tool-prefix)gcc-cpp-${SLOT}  \
    CXX=$(exhost --tool-prefix)g++-${SLOT}      \
    default
}

libgomp_src_install() {
    default

    nonfatal edo rmdir "${IMAGE}"/usr/$(exhost --target)/lib/gcc/$(exhost --target)/${PV}/finclude

    symlink_dynamic_libs ${PN}
    slot_dynamic_libs ${PN}
    slot_other_libs ${PN}.{a,la,spec}

    alternatives_for _${PN} ${SLOT} ${SLOT} \
        /usr/share/info/${PN}.info ${PN}-${SLOT}.info
}

