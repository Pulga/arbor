# Copyright 2007 Bryan Østergaard <kloeri@exherbo.org>
# Copyright 2008 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Public License v2

require bash-completion glib

SLOT="2"
PLATFORMS="~amd64 ~arm ~armv7 ~x86"
MYOPTIONS="
    gtk-doc
    ( providers: elfutils libelf ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        sys-devel/gettext[>=0.11]
        (
            app-text/docbook-xml-dtd:4.1.2 [[ note = [ Checked in configure.ac but not used ] ]]
            app-text/docbook-xsl-stylesheets
            dev-libs/libxslt
        ) [[ description = [ For man pages ] ]]
        gtk-doc? ( dev-doc/gtk-doc[>=1.20] )
    build+run:
        dev-libs/libffi[>=3.0.0]
        dev-libs/pcre[>=8.13]
        sys-apps/util-linux[>=2.28.1]
        providers:elfutils? ( dev-util/elfutils[>=0.142] )
        providers:libelf? ( dev-libs/libelf[>=0.8.12] )
    test:
        dev-util/desktop-file-utils
        gnome-bindings/pygobject:3
        sys-apps/dbus[>=1.2.14] [[
            note = [ Required for building GDBus tests ]
        ]]
        virtual/pkg-config[>=0.16]
        x11-misc/shared-mime-info
    recommendation:
        dev-libs/glib-networking [[ description = [ Required for GNet ] ]]
        sys-apps/dbus[>=1.2.14] [[ note = [ Required for GDBus ] ]]
    suggestion:
        gnome-desktop/dconf [[ description = [ default GSettings backend ] ]]
        gnome-desktop/gvfs [[ description = [ Provides utilities and support for GIO ] ]]
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/${PN}-gdbus-codegen-Don-t-assume-bindir-and-datadir-share-.patch
)

# NOTE: glib:2 is used by sydbox, and thus in system. For this reason it shouldn't hard depend on
# anything not in ::arbor, that means no recommendations, just suggestions

# Disable FAM support as FAM is no longer necessary with GIO's inotify support
#
# xattr support is also provided by libc.  glib will prefer to use glibc's xattr support over
# libattr.  As a result, there is no dependency added for attr support.
#
# Enable libelf (via elf-utils) to read compressed module data
DEFAULT_SRC_CONFIGURE_PARAMS=( '--prefix=/usr' "--exec_prefix=/usr/$(exhost --target)"
                               "--includedir=/usr/$(exhost --target)/include"
                               '--enable-static' '--disable-fam' '--disable-selinux'
                               '--enable-xattr' '--enable-libelf' '--with-pcre=system'
                               '--with-threads=posix' '--disable-dtrace' '--disable-systemtap'
                               '--disable-gcov' '--enable-man' '--enable-mount' )
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=( gtk-doc )
DEFAULT_SRC_CONFIGURE_TESTS=( '--enable-modular-tests --disable-modular-tests' )

src_prepare() {
    edo cp "${WORKBASE}"/pkg-config-*/pkg.m4 "${WORKBASE}"/
    default

    # fail on x86 due to floating point rounding errors
    # see https://bugzilla.gnome.org/show_bug.cgi?id=722604
    edo sed -e ':/timer/stop:d' \
            -e ':/timer/basic:d' \
            -i glib/tests/timer.c

    # use inet:127.0.0.1:31337 instead of inet:0.0.0.0@0
    edo sed -e 's:new_any (G_SOCKET_FAMILY_IPV4):new_from_string ("127.0.0.1"):' \
            -e '/g_inet_socket_address_new (addr, 0)/s:0:31337:' \
            -i gio/tests/socket.c

    # tries to access to root's dconf stuff
    edo sed -e ':/network-monitor/default:d'         \
            -e ':/network-monitor/add_networks:d'    \
            -e ':/network-monitor/remove_networks:d' \
            -i gio/tests/network-monitor.c

    # Running gdb under sydbox is problematic, exit before the gdb test
    edo sed -e '/type gdb/iexit 0' -i tests/run-assert-msg-test.sh
}

src_configure() {
    if ! exhost --is-native -q ; then
        # "Whether the stack grows up or down; defaults to "no", which works
        # most places. If you are compiling for PA-RISC or various other
        # architectures, you'll have to change this."
        export glib_cv_stack_grows=no
        # "Whether an underscore needs to be prepended to symbols when
        # looking them up via dlsym. Only needs to be set if your system
        # uses dlopen/dlsym."
        export glib_cv_uscore=yes
        # "Whether you have a getpwuid_r function (in your C library,
        # not your thread library) that conforms to the POSIX spec.
        # (Takes a 'struct passwd **' as the final argument)"
        export ac_cv_func_posix_getpwuid_r=yes
        # Whether you have a getgrgid_r function that conforms to the
        # POSIX spec.
        export ac_cv_func_posix_getgrgid_r=yes
    fi

    default
}

src_install() {
    default
    keepdir "/usr/$(exhost --target)/lib/gio/modules"

    if option !bash-completion ; then
        edo rm "${IMAGE}"/usr/share/bash-completion/completions/{gapplication,gdbus,gresource,gsettings}
        edo rmdir "${IMAGE}"/usr/share/bash-completion/{completions/,}
    fi
}

