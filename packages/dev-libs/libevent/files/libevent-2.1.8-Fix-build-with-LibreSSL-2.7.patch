Upstream: yes

From 28b8075400c70b2d2da2ce07e590c2ec6d11783d Mon Sep 17 00:00:00 2001
From: Bernard Spil <brnrd@FreeBSD.org>
Date: Mon, 2 Apr 2018 13:18:27 +0200
Subject: [PATCH] Fix build with LibreSSL 2.7

LibreSSL 2.7 implements OpenSSL 1.1 API except for BIO_get_init()

See also: https://bugs.freebsd.org/226900
Signed-off-by: Bernard Spil <brnrd@FreeBSD.org>
Closes: #617 (cherry-pick)
---
 openssl-compat.h                     | 10 ++++++++--
 sample/https-client.c                |  9 ++++++---
 sample/le-proxy.c                    |  3 ++-
 sample/openssl_hostname_validation.c |  3 ++-
 test/regress_ssl.c                   |  3 ++-
 5 files changed, 20 insertions(+), 8 deletions(-)

diff --git a/openssl-compat.h b/openssl-compat.h
index bffd2593..5d91ac64 100644
--- a/openssl-compat.h
+++ b/openssl-compat.h
@@ -4,7 +4,8 @@
 #include <openssl/bio.h>
 #include "util-internal.h"
 
-#if (OPENSSL_VERSION_NUMBER < 0x10100000L) || defined(LIBRESSL_VERSION_NUMBER)
+#if (OPENSSL_VERSION_NUMBER < 0x10100000L) || \
+	(defined(LIBRESSL_VERSION_NUMBER) && LIBRESSL_VERSION_NUMBER < 0x20700000L)
 
 static inline BIO_METHOD *BIO_meth_new(int type, const char *name)
 {
@@ -33,6 +34,11 @@ static inline BIO_METHOD *BIO_meth_new(int type, const char *name)
 
 #define TLS_method SSLv23_method
 
-#endif /* (OPENSSL_VERSION_NUMBER < 0x10100000L) || defined(LIBRESSL_VERSION_NUMBER) */
+#endif /* (OPENSSL_VERSION_NUMBER < 0x10100000L) || \
+	(defined(LIBRESSL_VERSION_NUMBER) && LIBRESSL_VERSION_NUMBER < 0x20700000L) */
+
+#if defined(LIBRESSL_VERSION_NUMBER) && LIBRESSL_VERSION_NUMBER >= 0x20700000L
+#define BIO_get_init(b) (b)->init
+#endif
 
 #endif /* OPENSSL_COMPAT_H */
diff --git a/sample/https-client.c b/sample/https-client.c
index 8ef75987..4292f094 100644
--- a/sample/https-client.c
+++ b/sample/https-client.c
@@ -312,7 +312,8 @@ main(int argc, char **argv)
 	}
 	uri[sizeof(uri) - 1] = '\0';
 
-#if (OPENSSL_VERSION_NUMBER < 0x10100000L) || defined(LIBRESSL_VERSION_NUMBER)
+#if (OPENSSL_VERSION_NUMBER < 0x10100000L) || \
+	(defined(LIBRESSL_VERSION_NUMBER) && LIBRESSL_VERSION_NUMBER < 0x20700000L)
 	// Initialize OpenSSL
 	SSL_library_init();
 	ERR_load_crypto_strings();
@@ -488,7 +489,8 @@ cleanup:
 		SSL_CTX_free(ssl_ctx);
 	if (type == HTTP && ssl)
 		SSL_free(ssl);
-#if (OPENSSL_VERSION_NUMBER < 0x10100000L) || defined(LIBRESSL_VERSION_NUMBER)
+#if (OPENSSL_VERSION_NUMBER < 0x10100000L) || \
+	(defined(LIBRESSL_VERSION_NUMBER) && LIBRESSL_VERSION_NUMBER < 0x20700000L)
 	EVP_cleanup();
 	ERR_free_strings();
 
@@ -501,7 +503,8 @@ cleanup:
 	CRYPTO_cleanup_all_ex_data();
 
 	sk_SSL_COMP_free(SSL_COMP_get_compression_methods());
-#endif /* (OPENSSL_VERSION_NUMBER < 0x10100000L) || defined(LIBRESSL_VERSION_NUMBER) */
+#endif /* (OPENSSL_VERSION_NUMBER < 0x10100000L) || \
+	(defined(LIBRESSL_VERSION_NUMBER) && LIBRESSL_VERSION_NUMBER < 0x20700000L) */
 
 #ifdef _WIN32
 	WSACleanup();
diff --git a/sample/le-proxy.c b/sample/le-proxy.c
index 4e1cf033..73c48881 100644
--- a/sample/le-proxy.c
+++ b/sample/le-proxy.c
@@ -261,7 +261,8 @@ main(int argc, char **argv)
 
 	if (use_ssl) {
 		int r;
-#if (OPENSSL_VERSION_NUMBER < 0x10100000L) || defined(LIBRESSL_VERSION_NUMBER)
+#if (OPENSSL_VERSION_NUMBER < 0x10100000L) || \
+	(defined(LIBRESSL_VERSION_NUMBER) && LIBRESSL_VERSION_NUMBER < 0x20700000L)
 		SSL_library_init();
 		ERR_load_crypto_strings();
 		SSL_load_error_strings();
diff --git a/sample/openssl_hostname_validation.c b/sample/openssl_hostname_validation.c
index a60e38c4..4036ccba 100644
--- a/sample/openssl_hostname_validation.c
+++ b/sample/openssl_hostname_validation.c
@@ -48,7 +48,8 @@ SOFTWARE.
 
 #define HOSTNAME_MAX_SIZE 255
 
-#if (OPENSSL_VERSION_NUMBER < 0x10100000L) || defined(LIBRESSL_VERSION_NUMBER)
+#if (OPENSSL_VERSION_NUMBER < 0x10100000L) || \
+	(defined(LIBRESSL_VERSION_NUMBER) && LIBRESSL_VERSION_NUMBER < 0x20700000L)
 #define ASN1_STRING_get0_data ASN1_STRING_data
 #endif
 
diff --git a/test/regress_ssl.c b/test/regress_ssl.c
index 5004f7e8..92026976 100644
--- a/test/regress_ssl.c
+++ b/test/regress_ssl.c
@@ -185,7 +185,8 @@ get_ssl_ctx(void)
 void
 init_ssl(void)
 {
-#if (OPENSSL_VERSION_NUMBER < 0x10100000L) || defined(LIBRESSL_VERSION_NUMBER)
+#if (OPENSSL_VERSION_NUMBER < 0x10100000L) || \
+	(defined(LIBRESSL_VERSION_NUMBER) && LIBRESSL_VERSION_NUMBER < 0x20700000L)
 	SSL_library_init();
 	ERR_load_crypto_strings();
 	SSL_load_error_strings();
-- 
2.17.1

