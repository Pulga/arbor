# Copyright 2018 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require sourceforge [ project=ed448goldilocks suffix=tgz ]

SUMMARY="Library implementing Ed448-Goldilocks elliptic curve and Ed25519"

LICENCES="
    BSD-2 [[ note = [ python/edgold/ed448.py ] ]]
    MIT
"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build:
        dev-lang/python:*[<3]
"

src_prepare() {
    # build scripts don't work with Python 3, last checked: 1.0.0
    export PYTHON=/usr/$(exhost --build)/bin/python2

    # do not strip by default and remove -Werror
    edo sed \
        -e '/strip/d' \
        -e 's:-Werror ::g' \
        -i Makefile

    default
}

src_compile() {
    emake lib
}

src_install() {
    dolib.so build/lib/*

    insinto /usr/$(exhost --target)/include
    doins -r src/GENERATED/include/*

    emagicdocs
}

