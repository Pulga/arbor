# Copyright 2008 Bryan Østergaard <kloeri@exherbo.org>
# Copyright 2008, 2009, 2010, 2016 Ingmar Vanhassel <ingmar@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'subversion-1.4.6-r2.ebuild' from Gentoo, which is:
#     Copyright 1999-2008 Gentoo Foundation.

require bash-completion \
        perl-module \
        python [ blacklist=none multibuild=false with_opt=true ] \
        berkdb \
        ruby [ multibuild=false with_opt=true ] \
        autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ none ] ]

export_exlib_phases src_configure src_compile src_install src_prepare src_test_expensive

SUMMARY="A free/open source, centralised version control system"
HOMEPAGE="https://${PN}.apache.org"
DOWNLOADS="mirror://apache/subversion/${PNV}.tar.bz2"

UPSTREAM_CHANGELOG="http://svn.apache.org/repos/asf/${PN}/tags/${PV}/CHANGES"
UPSTREAM_RELEASE_NOTES="${HOMEPAGE}/docs/release-notes/$(ever range 1-2).html"

LICENCES="Apache-2.0"
SLOT="0"
MYOPTIONS="
    apache
    berkdb
    keyring [[ description = [ Adds support for storing passwords in GNOME Keyring ] ]]
    kwallet [[ description = [ Adds support for storing passwords in KWallet ] ]]
    perl
    python
"

# Expensive tests
RESTRICT="test"

DEPENDENCIES="
    build:
        dev-lang/python:*[>=2.7]
        sys-devel/gettext
        keyring? ( virtual/pkg-config )
        kwallet? (
            kde-frameworks/kdelibs4support:5
            virtual/pkg-config
        )
        perl? ( dev-lang/swig[perl] )
        python? ( dev-lang/swig[python] )
        ruby? ( dev-lang/swig[ruby][ruby_abis:*(-)?] )
    build+run:
        app-arch/lz4
        dev-db/sqlite:3[>=3.8.11.1] [[ note = [ recommended version ] ]]
        dev-libs/apr:1[>=1.3.0]
        dev-libs/apr-util:1[>=0.9.7][berkdb?]
        dev-libs/expat
        dev-libs/utf8proc
        net-libs/serf[>=1.3.4]
        apache? ( www-servers/apache )
        berkdb? ( sys-libs/db:=[>=4.0.14&<4.9] )
        keyring? (
            dev-libs/glib:2
            dev-libs/libsecret:1
            sys-apps/dbus
        )
        kwallet? (
            kde-frameworks/kcoreaddons:5
            kde-frameworks/ki18n:5
            kde-frameworks/kwallet:5
            sys-apps/dbus[>=1.0]
            x11-libs/qtbase:5
        )
        perl? ( dev-lang/perl:= )
    run:
        keyring? ( gnome-desktop/gnome-keyring:1 )
        perl? ( dev-perl/URI )
    suggestion:
        net-misc/openssh [[ description = [ Enables support for svn+ssh:// URIs ] ]]
    test-expensive:
        dev-lang/python:*[>=2.7]
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/${PN}-perl-vendor.patch
    "${FILES}"/${PN}-perl-ccopts.patch
    "${FILES}"/0001-Don-t-underlink-perl-module.patch
)

subversion_src_prepare() {
    default

    edo sed -e '/AC_PATH_PROG(PKG_CONFIG/d' -i configure.ac

    eautoconf # Doesn't use automake
}

subversion_src_configure() {
    myconf=()

    if option perl || option python || option ruby; then
        myconf+=( --with-swig )
    else
        myconf+=( --without-swig )
    fi

    # local library preloading makes sure tests do not pick up on a version of svn that is already
    # installed
    RUBY=ruby$(ruby_get_abi) \
    econf \
        ${myconf} \
        --enable-local-library-preloading \
        --enable-nls \
        --disable-disallowing-of-undefined-references \
        --disable-ev2-impl \
        --disable-googlemock \
        --disable-javahl \
        --disable-mod-activation \
        --disable-static \
        --without-jikes \
        --without-junit \
        --without-old-gnome-keyring \
        --without-sasl \
        $(option_with apache apxs) \
        $(option_with berkdb berkeley-db $(option berkdb &&
            echo "db.h:$(berkdb_includedir)::db-$(berkdb_slot)")) \
        $(option_with keyring gnome-keyring) \
        $(option_with kwallet)
}

subversion_src_compile() {
    default
    option perl && emake swig-pl
    option python && emake swig-py
    option ruby && emake swig-rb
}

subversion_src_test_expensive() {
    emake check
}

subversion_src_install() {
    default

    if option perl; then
        emake DESTDIR="${IMAGE}" INST_MAN3DIR="${IMAGE}"/usr/share/man/man3 install-swig-pl
        fixlocalpod

        # We don't need packlists
        edo find "${IMAGE}/usr/$(exhost --target)/lib" -type f -name .packlist -delete
        # Duplicate man pages
        edo rm -rf "${IMAGE}"/usr/$(exhost --target)/man
        # Remove useless file
        edo rm "${IMAGE}"/usr/share/man/man3/.exists
    fi

    if option python; then
        emake DESTDIR="${IMAGE}" DISTUTIL_PARAM="--prefix=${IMAGE}" LD_LIBRARY_PATH="-L${IMAGE}/usr/$(exhost --target)/lib" install-swig-py

        dodir $(python_get_sitedir)
        edo mv "${IMAGE}"/usr/$(exhost --target)/lib/svn-python/{,lib}svn "${IMAGE}"/$(python_get_sitedir)
        edo rm -rf "${IMAGE}"/usr/$(exhost --target)/lib/svn-python
    fi

    if option ruby; then
        emake DESTDIR="${IMAGE}" install-swig-rb
    fi

    dobashcompletion tools/client-side/bash_completion
}

