# Copyright 2008 Bo Ørsted Andresen <zlin@exherbo.org>
# Copyright 2018 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require cmake [ api=2 ]

SUMMARY="C Library for manipulating zip archives"
DESCRIPTION="
A C library for reading, creating, and modifying zip archives. Files can be added from data
buffers, files, or compressed data copied directly from other zip archives. Changes made without
closing the archive can be reverted. Decryption and encryption of Winzip AES and decryption of
legacy PKware encrypted files is supported. The API is documented by man pages.
"
HOMEPAGE="https://www.nih.at/${PN}"
DOWNLOADS="${HOMEPAGE}/${PNV}.tar.xz"

UPSTREAM_RELEASE_NOTES="${HOMEPAGE}/NEWS.html [[ lang = en ]]"

LICENCES="BSD-3"
SLOT="0"
PLATFORMS="~amd64 ~armv7 ~armv8 ~x86"
MYOPTIONS="
    ( providers: gnutls libressl openssl ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        sys-apps/groff
    build+run:
        app-arch/bzip2
        sys-libs/zlib[>=1.1.2]
        providers:gnutls? (
            dev-libs/gnutls
            dev-libs/nettle
        )
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl )
"

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DBUILD_SHARED_LIBS:BOOL=TRUE
    -DENABLE_COMMONCRYPTO:BOOL=TRUE
)

CMAKE_SRC_CONFIGURE_OPTION_ENABLES=(
    'providers:gnutls GNUTLS'
    '!providers:gnutls OPENSSL'
)

